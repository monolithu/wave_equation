#include <stdio.h>
#include <math.h>
#include <time.h>

// compile with gcc -Wall -O4 wave_eq_c_seq.c -o wave_eq_c_seq -lm
// run with ./wave_eq_c
int main(void) {
    int n = 4096 * 100;
    int iters = 10000;
    double dx = 1.0;
    double dt = 0.5;
    double f = dt / (dx * dx);

    double u[n];
    double v[n];

    // initial conditions
    for(int i = 0; i < n; ++i) {
        u[i] = exp(-pow(i - n/2, 2.0) / (2.0 * 400.0));
        v[i] = 0.0;
    }
    
    
    clock_t start = clock(), diff;
    // iterations
    for(int it = 0; it < iters; ++it) {
        for(int i = 1; i < n-1; ++i) {   
            v[i] += f * (u[i-1] + u[i+1] - 2 * u[i]);
        }
        v[0]   += f * (u[n-1] + u[1] - 2 * u[0]);
        v[n-1] += f * (u[n-2] + u[0] - 2 * u[n-1]);
        
        for(int i = 0; i < n; ++i) {   
            u[i] += f * v[i];
        }
    }
    diff = clock() - start;
    int msec = diff * 1000 * 1000 / CLOCKS_PER_SEC;
    printf("Pure C: %f s\n", msec/(1000.0 * 1000.0));
    
}
