Testing Cython, parallelized Cython, C and C+OpenMP with the one-dimensional wave equation using leap-frog iteration.

**Example results on my machine**

*4096 grid points, 10^6 iterations*

```
Numpy  14.54 s
Sequential Cython 5.31 s
Parallel Cython 3.02 s
Pure C: 14.67 s
Pure C + OpenMP: 2.51 s
```

*409600 grid points, 10^4 iterations*

```
Numpy  26.06 s
Sequential Cython 5.87 s
Parallel Cython 2.18 s
Pure C: 13.86 s
Pure C + OpenMP: 1.82 s
```

My C code sucks, but why?