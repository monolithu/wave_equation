cimport cython
import cython
from cython.parallel cimport prange, parallel

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_parallel(double[::1] v, double[::1] u, double dt, double dx, int iterations):
    cdef int i
    cdef int it
    cdef int n = u.shape[0]
    cdef double f = dt / (dx * dx)
    with nogil, parallel(num_threads=8):
        for it in range(iterations):
            for i in prange(1, n-1):
                v[i] += f * (u[i-1] + u[i+1] - 2 * u[i])

            if cython.parallel.threadid() == 0:
                v[0]   += f * (u[n-1] + u[1] - 2 * u[0])
                v[n-1] += f * (u[n-2] + u[0] - 2 * u[n-1])

            for i in prange(0, n):
                u[i] += dt * v[i]

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def evolve_sequential(double[::1] v, double[::1] u, double dt, double dx, int iterations):
    cdef int i
    cdef int it
    cdef int n = u.shape[0]
    cdef double f = dt / (dx * dx)
    for it in range(iterations):
        for i in range(1, n-1):
            v[i] += f * (u[i-1] + u[i+1] - 2 * u[i])
        v[0]   += f * (u[n-1] + u[1] - 2 * u[0])
        v[n-1] += f * (u[n-2] + u[0] - 2 * u[n-1])

        for i in range(0, n):
            u[i] += dt * v[i]

