def evolve_numpy(v, u, dt, dx, iterations):
    f = dt / (dx * dx)
    n = u.shape[0]
    for it in range(iterations):
        v[1:n-1:] += f * (u[0:n-2:] + u[2:n:] - 2 * u[1:n-1:])
        v[0] += f * (u[-1] + u[1] - 2 * u[0])
        v[-1] += f * (u[-2] + u[0] - 2 * u[-1])
        u += dt * v
