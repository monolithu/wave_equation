import numpy as np
import time
import matplotlib.pyplot as plt
import pyximport
pyximport.install(reload_support=True)

import wave_eq_cython
import wave_eq_numpy


def init():
    n = 2 ** 12 * 100
    u = np.zeros(n, dtype=np.double)
    v = np.zeros(n, dtype=np.double)

    # initial conditions
    for x in range(n):
        u[x] = np.exp(- (x - n / 2) ** 2 / (2.0 * 400))
    return u, v

dt = np.double(0.5)
dx = np.double(1.0)

# run numpy code
u, v = init()
t = time.time()
wave_eq_numpy.evolve_numpy(v, u, dt, dx, 10000)
print("Numpy ", time.time() - t, "s")

# run sequential cython code
u, v = init()
t = time.time()
wave_eq_cython.evolve_sequential(v, u, dt, dx, 10000)
print("Sequential Cython", time.time() - t, "s")

# run parallel cython code
u, v = init()
t = time.time()
wave_eq_cython.evolve_parallel(v, u, dt, dx, 10000)
print("Parallel Cython", time.time() - t, "s")

plt.plot(u)
plt.ylabel('')
plt.show()
