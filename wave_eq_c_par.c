#include <stdio.h>
#include <math.h>
#include <time.h>
#include <omp.h>

// compile with gcc -Wall -O4 wave_eq_c_par.c -o wave_eq_c_par -lm -fopenmp
// run with ./wave_eq_c_par
int main(void) {
    int n = 4096 * 100;
    int iters = 10000;
    double dx = 1.0;
    double dt = 0.5;
    double f = dt / (dx * dx);

    double u[n];
    double v[n];

    // initial conditions
    for(int i = 0; i < n; ++i) {
        u[i] = exp(-pow(i - n/2, 2.0) / (2.0 * 400.0));
        v[i] = 0.0;
    }
    
    
    double start = omp_get_wtime();
    #pragma omp parallel
    // iterations
    for(int it = 0; it < iters; ++it) {
        #pragma omp for
        for(int i = 1; i < n-1; ++i) {   
            v[i] += f * (u[i-1] + u[i+1] - 2 * u[i]);
        }
            
        if(omp_get_thread_num() == 0) {
            v[0]   += f * (u[n-1] + u[1] - 2 * u[0]);
            v[n-1] += f * (u[n-2] + u[0] - 2 * u[n-1]);
        }
        
        #pragma omp for
        for(int i = 0; i < n; ++i) {   
            u[i] += f * v[i];
        }
    }
    double diff = omp_get_wtime() - start;
    printf("Pure C + OpenMP: %f s\n", diff);
    
}
